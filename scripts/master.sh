#!/bin/bash -ex

sudo kubeadm init --pod-network-cidr=10.0.10.0/24 --apiserver-advertise-address "10.0.0.10"
sudo kubeadm token create --print-join-command > /vagrant/join.sh

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
kubectl get pods --all-namespaces

cp $HOME/.kube/config /vagrant/kube-config

wget --quiet https://docs.projectcalico.org/manifests/tigera-operator.yaml
kubectl apply -f tigera-operator.yaml
wget --quiet https://docs.projectcalico.org/manifests/custom-resources.yaml
sed -i 's/26/24/' custom-resources.yaml
sed -i 's/192.168.0.0\/16/10.0.10.0\/24/' custom-resources.yaml
kubectl apply -f custom-resources.yaml

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.35.0/deploy/static/provider/baremetal/deploy.yaml

sudo apt-get install -y nfs-kernel-server
sudo mkdir /opt/sfw
sudo chmod 1777 /opt/sfw
sudo bash -c 'echo software > /opt/sfw/hello.txt'
sudo bash -c 'echo "/opt/sfw/ *(rw,sync,no_root_squash,subtree_check)" > /etc/exports'
sudo exportfs -ra
